import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  constructor(
    //1.Membuat dialog:
    public dialogRef: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public api: ApiService
  ) { }

  ngOnInit(): void {
  }

  loading!: boolean;
  //Membuat fungsi untuk save data:
  saveData() {
    this.loading = true;
    if (this.data.id == undefined) {
      //Prosedur untuk pengiriman data ke server menggunakan metode POST
      this.api.post('books', this.data).subscribe(result => {
        //Tutup dialog dan kirimkan feedback server ke fungsi pemanggil dialog
        this.dialogRef.close(result);
        this.loading = false;
      }, error => {
        this.loading = false;
        alert('Tidak dapat menyimpan data');
      });
    } else {
      //prosedur edit data menggunakan metode PUT 
      this.api.put('books/' + this.data.id, this.data).subscribe(result => {
        //tutup dialog dan kirimkan feedback server ke fungsi pemanggil dialog
        this.dialogRef.close(result);
        this.loading = false;
      }, error => {
        this.loading = false;
        alert('Tidak dapat memperbarui data');
      })
    }

  }

}
