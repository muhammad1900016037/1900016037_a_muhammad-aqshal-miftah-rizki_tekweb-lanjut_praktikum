import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent implements OnInit {

  constructor(
    public api: ApiService,
    public dialogRef: MatDialogRef<FileUploaderComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) { }

  ngOnInit(): void {
    console.log(this.dialogData);
  }

  //membuat fungsi untuk seleksi file:
  selectedFile: any;
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0];
      if (this.selectedFile.type != 'application/pdf') alert('File harus berekstensi PDF!');
      console.log(this.selectedFile);
    }
  }

  //membuat fungsi untuk upload file:
  loadingUpload!: boolean;
  uploadFile() {
    let input = new FormData();
    input.append('file', this.selectedFile);
    this.loadingUpload = true;
    this.api.upload(input).subscribe(data => {
      //lakukan update data produk disini
      this.updateProduct(data);
      //
      console.log(data);
    }, error => {
      this.loadingUpload = false;
      alert('Gagal mengunggah file');
    });
  }

  //membuat fungsi untuk update product:
  updateProduct(data: any) {
    if (data.status == true) {

      //lakukan update data produk di sini:
      this.updateBook(data);
      //

      alert('File berhasil diunggah');
      this.loadingUpload = false;
      this.dialogRef.close();
    } else {
      alert(data.message);
    }
  }

  //membuat fungsi untuk update buku:
  updateBook(data: any) {
    this.api.put('books/' + this.dialogData.id, { url: data.url }).subscribe(res => {
      console.log(res);
    })
  }

}
