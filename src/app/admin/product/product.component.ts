import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as FileSaver from 'file-saver';
import { ApiService } from 'src/app/services/api.service';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  //1.membuat title:
  title: any;

  //2.membuat koleksi book:
  book: any = {};

  //3.membuat koleksi books:
  books: any = [];
  constructor(
    public dialog: MatDialog,
    public api: ApiService
  ) {
  }

  ngOnInit(): void {
    //4.memanggil fungsi title:
    this.title = 'Products';

    //5.memanggil fungsi book:
    this.book = {
      title: 'Angular Tutorial for Beginners',
      author: 'Muhammad Aqshal Miftah Rizki',
      publisher: 'Ahmad Dahlan University',
      year: 2021,
      isbn: '1900016037',
      price: 65000
    };
    this.getBooks();
  }

  //membuat fungsi loading:
  loading!: boolean;

  //6.memanggil fungsi books:
  getBooks() {
    this.loading = true;
    this.api.get('bookswithauth').subscribe(result => {
      this.books = result;
      this.loading = false;
    }, error => {
      this.loading = false;
      alert('Ada masalah saat pengambilan data. Coba lagi!');
    })

    /*
    this.loading = true;
    this.api.get('books').subscribe(result => {
      this.books = result;
      this.loading = false;
    }, error => {
      this.loading = false;
      alert('Ada masalah saat pengambilan data. Coba lagi!');
    })
    */
  }

  //7. Membuat fungsi product detail:
  productDetail(data: any, idx: number) {
    let dialog = this.dialog.open(ProductDetailComponent, {
      width: '400px',
      data: data
    });

    dialog.afterClosed().subscribe(res => {
      if (res) {

        //jika idx=-1 (penambahan data baru) maka tambahkan data
        if (idx == -1) this.books.push(res);
        //jika tidak maka perbarui data  
        else this.books[idx] = data;
      }
    })
  }

  //Membuat fungsi loading:
  loadingDelete: any = {};

  //Membuat fungsi delete:
  deleteProduct(id: string, idx: any) {

    var conf = confirm('Delete item?');
    if (conf) {
      this.loadingDelete[idx] = true;
      this.api.delete('books/' + this.books[idx].id).subscribe(result => {
        this.books.splice(idx, 1);
        this.loadingDelete[idx] = false;
      }, error => {
        this.loadingDelete[idx] = false;
        alert('Tidak dapat menghapus data. Try again!');
      });
    }

  }

  //Membuat fungsi upload file:
  uploadFile(data: any) {
    let dialog = this.dialog.open(FileUploaderComponent, {
      width: '400px',
      data: data
    });

    dialog.afterClosed().subscribe(res => {
      return;
    })
  }

  //membuat fungsi download file:
  downloadFile(data: any) {
    FileSaver.saveAs('http://api.sunhouse.co.id/bookstore/' + data.url);
  }

}